import 'package:magicbook_app/core.dart';

class MainNavigationLoginService {
  List<MNMenuItemModel> loginItems = [
    MNMenuItemModel(
      label: 'dashboard variant 1',
      page: const LoginVariant1View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 2',
      page: const LoginVariant2View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 3',
      page: const LoginVariant3View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 4',
      page: const LoginVariant4View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 5',
      page: const LoginVariant5View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 6',
      page: const LoginVariant6View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 7',
      page: const LoginVariant7View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 8',
      page: const LoginVariant8View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 9',
      page: const LoginVariant9View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 10',
      page: const LoginVariant10View(),
    ),
  ];
}
