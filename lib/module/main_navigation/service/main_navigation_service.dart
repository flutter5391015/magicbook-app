import 'package:magicbook_app/module/dashboard/variant1/view/variant1_view.dart';
import 'package:magicbook_app/module/dashboard/variant10/view/variant10_view.dart';
import 'package:magicbook_app/module/dashboard/variant2/view/variant2_view.dart';
import 'package:magicbook_app/module/dashboard/variant3/view/variant3_view.dart';
import 'package:magicbook_app/module/dashboard/variant4/view/variant4_view.dart';
import 'package:magicbook_app/module/dashboard/variant5/view/variant5_view.dart';
import 'package:magicbook_app/module/dashboard/variant6/view/variant6_view.dart';
import 'package:magicbook_app/module/dashboard/variant7/view/variant7_view.dart';
import 'package:magicbook_app/module/dashboard/variant8/view/variant8_view.dart';
import 'package:magicbook_app/module/dashboard/variant9/view/variant9_view.dart';
import 'package:magicbook_app/module/main_navigation/models/mn_menu_item_model.dart';

class MainNavigationDashboardService {
  List<MNMenuItemModel> dashboardItems = [
    MNMenuItemModel(
      label: 'dashboard variant 1',
      page: const Variant1View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 2',
      page: const Variant2View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 3',
      page: const Variant3View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 4',
      page: const Variant4View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 5',
      page: const Variant5View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 6',
      page: const Variant6View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 7',
      page: const Variant7View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 8',
      page: const Variant8View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 9',
      page: const Variant9View(),
    ),
    MNMenuItemModel(
      label: 'dashboard variant 10',
      page: const Variant10View(),
    ),
  ];
}
