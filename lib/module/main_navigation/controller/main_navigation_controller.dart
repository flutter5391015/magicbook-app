import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';

class MainNavigationController extends State<MainNavigationView> {
  static late MainNavigationController instance;
  late MainNavigationView view;
  MainNavigationDashboardService mainNavigationDashboardService =
      MainNavigationDashboardService();

  MainNavigationLoginService mainNavigationLoginService =
      MainNavigationLoginService();

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
