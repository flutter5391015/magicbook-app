import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class MNMenuItemWidget extends StatelessWidget {
  final String title;
  final List<dynamic> menuList;
  const MNMenuItemWidget(
      {super.key, required this.title, required this.menuList});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: H4(title: title),
          ),
          SizedBox(
            height: 10.0.h,
          ),
          SingleChildScrollView(
            scrollDirection: Axis.horizontal,
            controller: ScrollController(),
            child: Row(
              children: List.generate(
                menuList.length,
                (index) {
                  final MNMenuItemModel item = menuList[index];
                  return InkWell(
                    onTap: () => Get.to(item.page!),
                    child: Container(
                      padding: EdgeInsets.symmetric(
                        horizontal: 24.0.h,
                        vertical: 30.0.w,
                      ),
                      margin: EdgeInsets.only(
                              left: (index == 0 ? 10 : 0).w, right: 20.0)
                          .w,
                      decoration: BoxDecoration(
                        color: Colors.white,
                        border: Border.all(
                          color: Colors.grey.shade300,
                        ),
                        borderRadius: BorderRadius.all(
                          Radius.circular(8.0.r),
                        ),
                      ),
                      child: Center(
                        child: Text(
                          "Variant ${index + 1}",
                          style: TextStyle(
                            color: Colors.black,
                            fontSize: 14.0.sp,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ),
        ],
      ),
    );
  }
}
