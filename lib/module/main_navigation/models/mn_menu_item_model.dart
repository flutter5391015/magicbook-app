import 'package:flutter/material.dart';

class MNMenuItemModel {
  String? label;
  Widget? page;

  MNMenuItemModel({this.label, this.page});
}
