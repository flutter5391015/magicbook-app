import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class MainNavigationView extends StatefulWidget {
  const MainNavigationView({Key? key}) : super(key: key);

  Widget build(context, MainNavigationController controller) {
    controller.view = this;

    return Scaffold(
      backgroundColor: Colors.grey.shade200,
      appBar: AppBar(
        centerTitle: false,
        title: const Text("Main Navigation"),
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          child: Column(
            children: [
              MNMenuItemWidget(
                title: 'Login',
                menuList: controller.mainNavigationLoginService.loginItems,
              ),
              SizedBox(
                height: 16.0.h,
              ),
              MNMenuItemWidget(
                title: 'Dashboard',
                menuList:
                    controller.mainNavigationDashboardService.dashboardItems,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<MainNavigationView> createState() => MainNavigationController();
}
