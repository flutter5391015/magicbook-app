import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant8_view.dart';

class LoginVariant8Controller extends State<LoginVariant8View> {
  static late LoginVariant8Controller instance;
  late LoginVariant8View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
