import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:magicbook_app/core.dart';

class LoginVariant7View extends StatefulWidget {
  const LoginVariant7View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant7Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: Container(
          width: MediaQuery.of(context).size.width,
          padding: const EdgeInsets.all(10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              const Spacer(),
              Center(
                child: Image.network(
                  "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                  width: 96.0.w,
                  height: 96.0.h,
                  fit: BoxFit.fill,
                ),
              ),
              const Spacer(),
              Container(
                margin: EdgeInsets.symmetric(horizontal: 20.w),
                width: MediaQuery.of(context).size.width,
                height: 350.h,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "Continue as",
                      style: TextStyle(
                        fontSize: 16.0.sp,
                        fontWeight: FontWeight.bold,
                        height: 22.sp / 16.sp,
                      ),
                    ),
                    SizedBox(
                      height: 7.0.h,
                    ),
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.only(
                                top: 24, left: 12, right: 12, bottom: 12)
                            .w,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                          color: Color(0xfffCDCDCD),
                          borderRadius: BorderRadius.circular(10).r,
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              child: Container(
                                child: ListView.builder(
                                  itemCount: 100,
                                  physics: const ScrollPhysics(),
                                  itemBuilder:
                                      (BuildContext context, int index) {
                                    return Container(
                                      margin: EdgeInsets.only(bottom: 12).h,
                                      width: MediaQuery.of(context).size.width,
                                      child: Row(
                                        mainAxisSize: MainAxisSize.min,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        crossAxisAlignment:
                                            CrossAxisAlignment.center,
                                        children: [
                                          Container(
                                            width: 250.w,
                                            height: 70.h,
                                            padding: EdgeInsets.all(10),
                                            decoration: BoxDecoration(
                                              color: Color(0xfff969FA2),
                                              borderRadius:
                                                  BorderRadius.circular(10).r,
                                            ),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.start,
                                              children: [
                                                CircleAvatar(
                                                  backgroundColor: Colors.white,
                                                  radius: 20.0.r,
                                                  child: Icon(
                                                    Icons.person,
                                                    color: Colors.grey[900]!,
                                                    size: 20.0.r,
                                                  ),
                                                ),
                                                SizedBox(
                                                  width: 14.0.w,
                                                ),
                                                Container(
                                                  constraints: BoxConstraints(
                                                    maxWidth: 119.w,
                                                  ),
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment
                                                            .start,
                                                    children: [
                                                      Expanded(
                                                        child: Text(
                                                          "Nada Ghaisani Anantari",
                                                          style: TextStyle(
                                                            fontSize: 20.0.sp,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            height: 30 / 20,
                                                            overflow:
                                                                TextOverflow
                                                                    .ellipsis,
                                                          ),
                                                        ),
                                                      ),
                                                      Text(
                                                        "nada.g@mail.io",
                                                        style: TextStyle(
                                                          fontSize: 14.0.sp,
                                                          fontWeight:
                                                              FontWeight.w400,
                                                          height: 22 / 14,
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ),
                                                const Spacer(),
                                                Icon(
                                                  Icons.navigate_next,
                                                  size: 34.h,
                                                )
                                              ],
                                            ),
                                          ),
                                          Icon(
                                            FontAwesomeIcons.trash,
                                            size: 20.h,
                                          )
                                        ],
                                      ),
                                    );
                                  },
                                ),
                              ),
                            ),
                            Text(
                              "+ Add Another Account",
                              style: TextStyle(
                                fontSize: 14.0.sp,
                                fontWeight: FontWeight.bold,
                                color: Color(0xfff59B58D),
                                height: 22.sp / 14.sp,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              ),
              const Spacer(
                flex: 3,
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: SafeArea(
        child: Container(
          margin: EdgeInsets.only(bottom: 16).h,
          height: 20.h,
          width: MediaQuery.of(context).size.width,
          alignment: Alignment.center,
          child: Wrap(
            spacing: 5,
            crossAxisAlignment: WrapCrossAlignment.center,
            children: [
              Text(
                "Not registered yet?",
                style: TextStyle(
                  fontSize: 17.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xffff969FA2),
                ),
              ),
              Text(
                "Create an Account",
                style: TextStyle(
                    fontSize: 17.0,
                    fontWeight: FontWeight.bold,
                    color: primaryColor),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant7View> createState() => LoginVariant7Controller();
}
