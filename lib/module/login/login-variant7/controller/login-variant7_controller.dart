import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant7_view.dart';

class LoginVariant7Controller extends State<LoginVariant7View> {
  static late LoginVariant7Controller instance;
  late LoginVariant7View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
