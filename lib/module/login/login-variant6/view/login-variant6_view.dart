import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';
import 'package:magicbook_app/module/login/login-variant6/widget/login-variant6_button.dart';
import 'package:magicbook_app/module/login/login-variant6/widget/login-variant6_buttonLogin.dart';

class LoginVariant6View extends StatefulWidget {
  const LoginVariant6View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant6Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              const Spacer(),
              Center(
                child: Image.network(
                  "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                  width: 96.0.w,
                  height: 96.0.h,
                  fit: BoxFit.fill,
                ),
              ),
              SizedBox(
                height: 31.0.h,
              ),
              Container(
                constraints: BoxConstraints(
                  maxHeight: MediaQuery.of(context).size.height - 450.h,
                ),
                child: ListView.builder(
                  itemCount: 10,
                  physics: const ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Color(0xfffF4F6F6),
                        radius: 20.r,
                        child: Icon(
                          Icons.person,
                          size: 20.0.r,
                          color: Color(0xfff455154),
                        ),
                      ),
                      minLeadingWidth: 0.0,
                      title: Text("John doe"),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          LoginVariant6ButtonLogin(
                            onTap: () => print(''),
                            label: 'Login',
                          ),
                          SizedBox(
                            width: 6.0.w,
                          ),
                          Icon(
                            Icons.more_vert,
                            size: 24.0,
                            color: Colors.grey[900]!,
                          ),
                        ],
                      ),
                    );
                  },
                ),
              ),
              const Spacer(),
            ],
          ),
        ),
      ),
      bottomNavigationBar: SafeArea(
        child: Container(
          margin: EdgeInsets.only(left: 22, right: 22, bottom: 16).w,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              LoginVariant6Button(
                  onTap: () => print('hallo work'), label: 'Switch Accounts'),
              SizedBox(
                width: 22.0.w,
              ),
              LoginVariant6Button(
                  onTap: () => print('hallo work'), label: 'Sign Up'),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant6View> createState() => LoginVariant6Controller();
}
