import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

class LoginVariant6ButtonLogin extends StatelessWidget {
  final void Function() onTap;
  final String label;
  const LoginVariant6ButtonLogin(
      {super.key, required this.onTap, required this.label});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 20.h,
      width: 52.w,
      decoration: BoxDecoration(
        color: Color(0xfffCDCDCD),
        borderRadius: BorderRadius.circular(4),
      ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          padding: EdgeInsets.all(0),
          backgroundColor: Colors.transparent,
          shadowColor: Colors.transparent,
          shape: BeveledRectangleBorder(
            borderRadius: BorderRadius.circular(4),
          ),
        ),
        onPressed: onTap,
        child: Text(
          label,
          style: TextStyle(
            fontSize: 10.0.sp,
            fontWeight: FontWeight.bold,
            color: Colors.grey[900]!,
          ),
        ),
      ),
    );
  }
}
