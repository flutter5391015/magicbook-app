import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant6_view.dart';

class LoginVariant6Controller extends State<LoginVariant6View> {
  static late LoginVariant6Controller instance;
  late LoginVariant6View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
