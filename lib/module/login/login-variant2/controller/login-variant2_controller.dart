import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant2_view.dart';

class LoginVariant2Controller extends State<LoginVariant2View> {
  static late LoginVariant2Controller instance;
  late LoginVariant2View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
