import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';
import '../controller/login-variant2_controller.dart';

class LoginVariant2View extends StatefulWidget {
  const LoginVariant2View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant2Controller controller) {
    controller.view = this;
    return Scaffold(
      body: Container(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            const Spacer(),
            Image.network(
              "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
              width: 48.0.w,
              height: 48.0.w,
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: 18.0.h,
            ),
            RichText(
              text: TextSpan(
                style: TextStyle(
                  fontSize: 36.0,
                  fontWeight: FontWeight.bold,
                  color: Colors.black,
                ),
                children: [
                  TextSpan(text: "Welcome\n"),
                  TextSpan(
                    text: "to Our App",
                    style: TextStyle(
                      fontSize: 36.0,
                      fontWeight: FontWeight.normal,
                      color: Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 18.0.h,
            ),
            Text(
              "Write less do more",
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w400,
                height: 16 / 22,
              ),
            ),
            const Spacer(),
          ],
        ),
      ),
      bottomNavigationBar: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              margin: EdgeInsets.only(
                left: 30,
                right: 30,
              ).w,
              child: Text(
                "Use Facebook to find your friends",
                style: TextStyle(
                  fontSize: 14.0,
                  color: Color(0xfff969FA2),
                ),
              ),
            ),
            SizedBox(
              height: 9.0.h,
            ),
            Container(
              margin: EdgeInsets.only(
                left: 30,
                right: 30,
                bottom: 20,
              ).w,
              height: 48.h,
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  enableFeedback: false,
                  backgroundColor: Colors.blueAccent,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(8.0),
                  ),
                  elevation: 0,
                  shadowColor: Colors.transparent,
                ),
                onPressed: () => print('hallow'),
                child: Row(
                  children: [
                    Text(
                      "Log In with Facebook",
                      style: TextStyle(
                        fontSize: 16.0.h,
                        fontWeight: FontWeight.bold,
                        color: Colors.white,
                      ),
                    ),
                    const Spacer(),
                    Icon(
                      Icons.facebook_rounded,
                      size: 24.0,
                      color: Colors.white,
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  @override
  State<LoginVariant2View> createState() => LoginVariant2Controller();
}
