import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant1_view.dart';

class LoginVariant1Controller extends State<LoginVariant1View> {
  static late LoginVariant1Controller instance;
  late LoginVariant1View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);

  bool isShowPassword = false;

  bool setShowPassword() {
    isShowPassword = !isShowPassword;
    setState(() {});
    return isShowPassword;
  }
}
