import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:magicbook_app/core.dart';

class LoginVariant1View extends StatefulWidget {
  const LoginVariant1View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant1Controller controller) {
    controller.view = this;
    //final double keyboardHeight = MediaQuery.of(context).viewInsets.bottom + 16.h; buat dapetin tinggi keyboard

    return GestureDetector(
      onTap: () => FocusHelper.unfocus(context),
      child: Scaffold(
        body: SafeArea(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 30, horizontal: 14).w,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      constraints: BoxConstraints(
                        maxWidth: MediaQuery.of(context).size.width - 150.w,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Welcome to Our App",
                            style: TextStyle(
                              fontSize: 36.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          SizedBox(
                            height: 18.0.h,
                          ),
                          Text(
                            "Write less do more",
                            style: TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.w400,
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(
                      width: 38.0.w,
                    ),
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                      width: 48.0.w,
                      height: 48.0.w,
                      fit: BoxFit.contain,
                    ),
                  ],
                ),
                const Spacer(),
                QTextField(
                  suffixIcon: Icons.email,
                  hint: 'Email',
                  validator: Validator.required,
                  onChanged: (value) {},
                ),
                QTextField(
                  hint: 'Password',
                  suffixIconButton: IconButton(
                    onPressed: () => controller.setShowPassword(),
                    icon: Icon(
                      controller.isShowPassword
                          ? FontAwesomeIcons.eyeSlash
                          : FontAwesomeIcons
                              .eye, // Menggunakan FontAwesomeIcons
                    ),
                  ),
                  obscure: controller.isShowPassword,
                  validator: Validator.required,
                  onChanged: (value) {},
                ),
                QButton(
                  label: 'Login',
                  onPressed: () => print('hallo'),
                ),
                const Spacer(),
              ],
            ),
          ),
        ),
        bottomNavigationBar: SafeArea(
          child: Container(
            margin: EdgeInsets.only(
              left: 30,
              right: 30,
              bottom: 20,
            ).w,
            height: 48.h,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                enableFeedback: false,
                backgroundColor: Color(0xfffcdcdcd),
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(8.0),
                ),
                elevation: 0,
                shadowColor: Colors.transparent,
              ),
              onPressed: () => print('hallow'),
              child: Row(
                children: [
                  Text(
                    "Sign Up",
                    style: TextStyle(
                      fontSize: 16.0.h,
                      fontWeight: FontWeight.bold,
                      color: Color(0xfff455154),
                    ),
                  ),
                  const Spacer(),
                  Icon(
                    Icons.chevron_right,
                    size: 24.0,
                    color: Color(0xfff969FA2),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant1View> createState() => LoginVariant1Controller();
}
