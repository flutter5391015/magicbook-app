import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class LoginVariant5View extends StatefulWidget {
  const LoginVariant5View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant5Controller controller) {
    controller.view = this;

    return Scaffold(
      extendBody: true,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(
              "https://images.unsplash.com/photo-1513171920216-2640b288471b?ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MjB8fHBlb3BsZXxlbnwwfHwwfHx8MA%3D%3D&auto=format&fit=crop&w=800&q=60",
            ),
            fit: BoxFit.cover,
          ),
        ),
        child: Container(
          decoration: BoxDecoration(
            color: Color(0xfffCDCDCD).withOpacity(0.7),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                child: Image.network(
                  "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                  width: 128.0.w,
                  height: 128.0.h,
                  fit: BoxFit.contain,
                ),
              ),
              SizedBox(
                height: 41.0.h,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                margin: EdgeInsets.symmetric(horizontal: 30).w,
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Log In with Google",
                    style: TextStyle(
                      fontSize: 18.0.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      height: 22 / 18,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                margin: EdgeInsets.symmetric(horizontal: 30).w,
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Log In with Facebook",
                    style: TextStyle(
                      fontSize: 18.0.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      height: 22 / 18,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0.h,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                margin: EdgeInsets.symmetric(horizontal: 30).w,
                decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Log In with Guest",
                    style: TextStyle(
                      fontSize: 18.0.sp,
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      height: 22 / 18,
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 10.0.h,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant5View> createState() => LoginVariant5Controller();
}
