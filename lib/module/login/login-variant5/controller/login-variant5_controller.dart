import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant5_view.dart';

class LoginVariant5Controller extends State<LoginVariant5View> {
  static late LoginVariant5Controller instance;
  late LoginVariant5View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
