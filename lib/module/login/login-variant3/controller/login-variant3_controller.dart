import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant3_view.dart';

class LoginVariant3Controller extends State<LoginVariant3View> {
  static late LoginVariant3Controller instance;
  late LoginVariant3View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
