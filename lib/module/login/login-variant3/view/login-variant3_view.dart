import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:magicbook_app/core.dart';
import '../controller/login-variant3_controller.dart';

class LoginVariant3View extends StatefulWidget {
  const LoginVariant3View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant3Controller controller) {
    controller.view = this;

    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              const Spacer(),
              Center(
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                      width: 96.0.w,
                      height: 96.0.w,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 28.0.h,
                    ),
                    Text(
                      "Write less do more",
                      style: TextStyle(fontSize: 16.0, height: 16 / 22),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              Text(
                "Login with social networks",
                style: TextStyle(
                  fontSize: 14.0,
                  color: Color(0xfff969FA2),
                  height: 14 / 18,
                ),
              ),
              SizedBox(
                height: 9.0.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    height: 48.h,
                    width: 150.w,
                    decoration: BoxDecoration(
                      color: Colors.green,
                      borderRadius: BorderRadius.all(
                        Radius.circular(4).r,
                      ),
                    ),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        enableFeedback: false,
                        backgroundColor: Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0).r,
                        ),
                        elevation: 0,
                        shadowColor: Colors.transparent,
                      ),
                      onPressed: () => print('facebook'),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Facebook",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              height: 18 / 22,
                            ),
                          ),
                          const Spacer(),
                          Icon(
                            FontAwesomeIcons.facebook,
                            size: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 15.0.w,
                  ),
                  Container(
                    height: 48.h,
                    width: 150.w,
                    decoration: BoxDecoration(
                        color: Colors.green,
                        borderRadius: BorderRadius.all(
                          Radius.circular(4).r,
                        )),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        enableFeedback: false,
                        backgroundColor: Colors.transparent,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(4.0).r,
                        ),
                        elevation: 0,
                        shadowColor: Colors.transparent,
                      ),
                      onPressed: () => print('Twitter'),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text(
                            "Twitter",
                            style: TextStyle(
                              fontSize: 18.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.white,
                              height: 18 / 22,
                            ),
                          ),
                          const Spacer(),
                          Icon(
                            FontAwesomeIcons.twitter,
                            size: 18,
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 21.0.h,
              ),
              Container(
                height: 48.h,
                margin: EdgeInsets.symmetric(horizontal: 20).w,
                decoration: BoxDecoration(
                  color: Color(0xfffF4F6F6),
                  borderRadius: BorderRadius.circular(8),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    enableFeedback: false,
                    backgroundColor: Colors.transparent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(4.0).r,
                    ),
                    elevation: 0,
                    shadowColor: Colors.transparent,
                  ),
                  onPressed: () => print('Sign Up'),
                  child: Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Text(
                        "Sign Up",
                        style: TextStyle(
                          fontSize: 18.0,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey[900]!,
                          height: 18 / 22,
                        ),
                      ),
                      const Spacer(),
                      Icon(
                        Icons.email,
                        size: 18,
                        color: Colors.grey[900]!,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30.0.h,
              ),
              Text(
                "Login with Email",
                style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.bold,
                  color: Color(0xfff59B58D),
                ),
              ),
              SizedBox(
                height: 16.0.h,
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant3View> createState() => LoginVariant3Controller();
}
