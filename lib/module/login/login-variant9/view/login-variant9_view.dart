import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../controller/login-variant9_controller.dart';

class LoginVariant9View extends StatefulWidget {
  const LoginVariant9View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant9Controller controller) {
    controller.view = this;

    return Scaffold(
      appBar: AppBar(
        title: const Text("LoginVariant9"),
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: const [],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant9View> createState() => LoginVariant9Controller();
}
