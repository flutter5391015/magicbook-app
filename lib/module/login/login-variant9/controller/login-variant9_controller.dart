import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant9_view.dart';

class LoginVariant9Controller extends State<LoginVariant9View> {
  static late LoginVariant9Controller instance;
  late LoginVariant9View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
