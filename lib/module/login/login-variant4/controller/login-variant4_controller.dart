import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant4_view.dart';

class LoginVariant4Controller extends State<LoginVariant4View> {
  static late LoginVariant4Controller instance;
  late LoginVariant4View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
