import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';
import '../controller/login-variant4_controller.dart';

class LoginVariant4View extends StatefulWidget {
  const LoginVariant4View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant4Controller controller) {
    controller.view = this;

    return SafeArea(
      child: Scaffold(
        body: Container(
          padding: const EdgeInsets.all(16.0),
          child: Column(
            children: [
              const Spacer(),
              Center(
                child: Column(
                  children: [
                    Image.network(
                      "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                      width: 96.0.w,
                      height: 96.0.w,
                      fit: BoxFit.cover,
                    ),
                    SizedBox(
                      height: 20.0.h,
                    ),
                    CircleAvatar(
                      backgroundColor: Color(0xfffF4F6F6),
                      radius: 50.0.r,
                      child: Icon(
                        Icons.person,
                        color: Colors.grey[900]!,
                        size: 50.0.w,
                      ),
                    ),
                    SizedBox(
                      height: 14.0.h,
                    ),
                    Text(
                      "Nada Ghaisani Anantari",
                      style: TextStyle(
                          fontSize: 30.0,
                          fontWeight: FontWeight.bold,
                          height: 30 / 34),
                    ),
                    SizedBox(
                      height: 8.0.h,
                    ),
                    Text(
                      "nada.g@mail.io",
                      style: TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.w400,
                        height: 16 / 22,
                      ),
                    ),
                  ],
                ),
              ),
              const Spacer(),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                decoration: BoxDecoration(
                  color: Colors.green,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Continue as Nada",
                    style: TextStyle(
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                        height: 18 / 22),
                  ),
                ),
              ),
              SizedBox(
                height: 30.0.h,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 48.h,
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(4),
                ),
                child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    backgroundColor: Colors.transparent,
                    shadowColor: Colors.transparent,
                    shape: BeveledRectangleBorder(
                      borderRadius: BorderRadius.circular(4),
                    ),
                  ),
                  onPressed: () {},
                  child: Text(
                    "Choose another account",
                    style: TextStyle(
                      fontSize: 18.0,
                      color: Colors.green,
                      fontWeight: FontWeight.bold,
                      height: 18 / 22,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant4View> createState() => LoginVariant4Controller();
}
