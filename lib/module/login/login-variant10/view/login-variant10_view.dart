import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../controller/login-variant10_controller.dart';

class LoginVariant10View extends StatefulWidget {
  const LoginVariant10View({Key? key}) : super(key: key);

  Widget build(context, LoginVariant10Controller controller) {
    controller.view = this;

    return Scaffold(
      appBar: AppBar(
        title: const Text("LoginVariant10"),
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: const [],
          ),
        ),
      ),
    );
  }

  @override
  State<LoginVariant10View> createState() => LoginVariant10Controller();
}
