import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../view/login-variant10_view.dart';

class LoginVariant10Controller extends State<LoginVariant10View> {
  static late LoginVariant10Controller instance;
  late LoginVariant10View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
