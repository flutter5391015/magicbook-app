import 'package:flutter/material.dart';
import '../view/variant3_view.dart';

class Variant3Controller extends State<Variant3View> {
  static late Variant3Controller instance;
  late Variant3View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
