import 'package:flutter/material.dart';
import '../view/variant5_view.dart';

class Variant5Controller extends State<Variant5View> {
  static late Variant5Controller instance;
  late Variant5View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
