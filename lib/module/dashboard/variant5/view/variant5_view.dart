import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';
import 'package:readmore/readmore.dart';

class Variant5View extends StatefulWidget {
  const Variant5View({Key? key}) : super(key: key);

  Widget build(context, Variant5Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  padding:
                      const EdgeInsets.only(left: 16.0, right: 16.0, top: 5.0)
                          .w,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                        width: 32.w,
                        height: 32.h,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Text(
                        "Company Name",
                        style: TextStyle(
                          fontSize: 20.0.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const Spacer(),
                      Icon(Icons.add),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Icon(Icons.search),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Icon(Icons.more_horiz),
                    ],
                  ),
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  controller: ScrollController(),
                  child: Row(
                    children: List.generate(
                      10,
                      (index) {
                        return Row(
                          children: [
                            if (index == 0) ...[
                              Container(
                                height: 72.h,
                                margin: EdgeInsets.only(left: 16, right: 16).w,
                                child: Column(
                                  children: [
                                    CircleAvatar(
                                      radius: 25.r,
                                      backgroundColor: primaryColor,
                                      child: Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 6.0.h,
                                    ),
                                    Text(
                                      "Add Story",
                                      style: TextStyle(
                                        fontSize: 12.0.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                            Container(
                              height: 72.h,
                              width: 54.w,
                              margin: EdgeInsets.only(right: 16).w,
                              child: Column(
                                children: [
                                  CircleAvatar(
                                    radius: 25.r,
                                    backgroundColor: Color(0xfffCDCDCD),
                                    child: Icon(
                                      Icons.person_2,
                                      color: Colors.grey[900]!,
                                    ),
                                  ),
                                  SizedBox(
                                    height: 6.0.h,
                                  ),
                                  Expanded(
                                    child: Text(
                                      "User ${index + 1}",
                                      maxLines: 1,
                                      style: TextStyle(
                                        overflow: TextOverflow.ellipsis,
                                        fontSize: 12.0.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        );
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                Container(
                  child: GridView.builder(
                    padding: EdgeInsets.zero,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 1.0,
                      crossAxisCount: 1,
                    ),
                    itemCount: 2,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      bool isEven = index % 2 == 0;
                      late Widget headerContainer;
                      late Widget bodyContainer;
                      if (isEven) {
                        headerContainer = Container(
                          child: Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Wrap(
                                  children: [
                                    Text(
                                      "Muhammad Charis Az",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4.0.w,
                                    ),
                                    Text(
                                      "with",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4.0.w,
                                    ),
                                    Text(
                                      "Nada Ghaisani",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.w700,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0.h,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "2 min ago",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 10.0.w,
                                    ),
                                    Icon(
                                      Icons.location_on,
                                      size: 15,
                                    ),
                                    SizedBox(
                                      width: 1.0.w,
                                    ),
                                    Text(
                                      "Semarang, ID",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );

                        bodyContainer = Container(
                          height: 166.0.h,
                          decoration: BoxDecoration(
                            color: Colors.grey[100]!,
                            image: DecorationImage(
                              image: NetworkImage(
                                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png",
                              ),
                              fit: BoxFit.cover,
                            ),
                            borderRadius: BorderRadius.all(
                              Radius.circular(
                                8.0,
                              ),
                            ),
                          ),
                        );
                      } else {
                        headerContainer = Container(
                          child: Expanded(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Wrap(
                                  children: [
                                    Text(
                                      "Muhammad Charis Az",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.bold,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4.0.w,
                                    ),
                                    Text(
                                      "is",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 4.0.w,
                                    ),
                                    Icon(
                                      Icons.sentiment_satisfied_alt,
                                      size: 20,
                                    ),
                                    SizedBox(
                                      width: 4.0.w,
                                    ),
                                    Text(
                                      "feeling happy",
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.w400,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ],
                                ),
                                SizedBox(
                                  height: 5.0.h,
                                ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Text(
                                      "3 hours ago",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        );

                        bodyContainer = Container(
                          child: ReadMoreText(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat",
                            trimLines: 4,
                            trimMode: TrimMode.Line,
                            colorClickableText: Colors.pink,
                            trimCollapsedText: 'Read More',
                            trimExpandedText: ' Less',
                            moreStyle: TextStyle(
                              color: Color(0xfff455154),
                              fontWeight: FontWeight.w400,
                              fontSize: 16.sp,
                            ),
                          ),
                        );
                      }

                      return Container(
                        padding: EdgeInsets.all(15).w,
                        decoration: BoxDecoration(
                            border: Border(
                          top: BorderSide(width: 10, color: Color(0xfffF3F6F6)),
                        )),
                        child: Column(
                          children: [
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: Color(0xfffCDCDCD),
                                  radius: 28.0,
                                  child: Icon(
                                    Icons.person_2,
                                    color: Colors.grey[900]!,
                                    size: 28.0,
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                headerContainer,
                                Icon(Icons.more_horiz),
                              ],
                            ),
                            SizedBox(
                              height: 16.0.h,
                            ),
                            bodyContainer,
                            const Spacer(),
                            Row(
                              children: [
                                Icon(Icons.favorite),
                                SizedBox(
                                  width: 5.0.w,
                                ),
                                Icon(Icons.forum),
                                const Spacer(),
                                Icon(Icons.upload_sharp),
                              ],
                            ),
                            SizedBox(
                              height: 5.0.h,
                            ),
                            Container(
                              height: 1.h,
                              color: Color(0xfffE6E9EB),
                            ),
                            SizedBox(
                              height: 10.0.h,
                            ),
                            Row(
                              children: [
                                Text(
                                  "831 likes",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                                SizedBox(
                                  width: 5.0.w,
                                ),
                                Text(
                                  "61 comments",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                                const Spacer(),
                                Text(
                                  "8 shares",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                              ],
                            ),
                          ],
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<Variant5View> createState() => Variant5Controller();
}
