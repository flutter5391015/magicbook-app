import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';
import '../controller/variant6_controller.dart';

class Variant6View extends StatefulWidget {
  const Variant6View({Key? key}) : super(key: key);

  Widget build(context, Variant6Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Container(
                  padding: const EdgeInsets.only(left: 16, right: 16, top: 5).w,
                  child: Column(
                    children: [
                      Container(
                        height: 44.h,
                        padding: EdgeInsets.symmetric(
                          horizontal: 1.0.w,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              6.0.r,
                            ),
                          ),
                        ),
                        child: Center(
                          child: TextField(
                            style: TextStyle(
                              backgroundColor: Colors.transparent,
                              color: Colors.grey[800],
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              enabledBorder: InputBorder.none,
                              hintText: "Search",
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey[800],
                              ),
                              suffixIcon: Icon(
                                Icons.mic,
                                color: Colors.grey[800],
                              ),
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16.0.h,
                      ),
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundColor: Colors.grey[300]!,
                            radius: 28.0,
                            child: Icon(
                              Icons.person,
                              color: Colors.grey[900]!,
                              size: 28.0,
                            ),
                          ),
                          SizedBox(
                            width: 10.0.w,
                          ),
                          Text(
                            "What’s new, Charis?",
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Color(0xfff969FA2),
                            ),
                          ),
                          const Spacer(),
                          CircleAvatar(
                            backgroundColor: Colors.grey[300]!,
                            radius: 20.0,
                            child: Icon(
                              Icons.image,
                              color: Colors.grey[900]!,
                              size: 20.0,
                            ),
                          ),
                          SizedBox(
                            width: 5.0.w,
                          ),
                          CircleAvatar(
                            backgroundColor: Colors.grey[300]!,
                            radius: 20.0,
                            child: Icon(
                              Icons.video_call,
                              color: Colors.grey[900]!,
                              size: 20.0,
                            ),
                          ),
                          SizedBox(
                            width: 5.0.w,
                          ),
                          CircleAvatar(
                            backgroundColor: Colors.grey[300]!,
                            radius: 20.0,
                            child: Icon(
                              Icons.room,
                              color: Colors.grey[900]!,
                              size: 20.0,
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                    ],
                  ),
                ),
                Container(
                  height: 2.h,
                  color: Color(0xfffE6E9EB),
                ),
                GridView.builder(
                  padding: EdgeInsets.zero,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    childAspectRatio: 1.0,
                    crossAxisCount: 1,
                  ),
                  itemCount: 2,
                  shrinkWrap: true,
                  physics: ScrollPhysics(),
                  itemBuilder: (BuildContext context, int index) {
                    late Widget bodyContainer;
                    if (index % 2 == 0) {
                      bodyContainer = Column(
                        children: [
                          Text(
                            "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                            maxLines: 2,
                            style: TextStyle(
                              overflow: TextOverflow.clip,
                              fontSize: 16.0,
                            ),
                          ),
                          SizedBox(
                            height: 10.0.h,
                          ),
                          Container(
                            height: 130.0,
                            decoration: BoxDecoration(
                              color: Colors.grey[100]!,
                              image: DecorationImage(
                                image: NetworkImage(
                                  "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png",
                                ),
                                fit: BoxFit.contain,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(
                                  8.0,
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 10.0.h,
                          ),
                          Row(
                            children: [
                              CircleAvatar(
                                backgroundColor: Colors.grey[300]!,
                                radius: 12.0,
                                child: Icon(
                                  Icons.person,
                                  color: Colors.grey[900]!,
                                  size: 12.0,
                                ),
                              ),
                              SizedBox(
                                width: 10.0.w,
                              ),
                              Wrap(
                                children: [
                                  Text(
                                    "Nada Ghaisani",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xfff455154),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 1.0.w,
                                  ),
                                  Text(
                                    "and",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Color(0xfff455154),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 1.0.w,
                                  ),
                                  Text(
                                    "3 others",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold,
                                      color: Color(0xfff455154),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 1.0.w,
                                  ),
                                  Text(
                                    "have been here",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                      color: Color(0xfff455154),
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      );
                    } else {
                      bodyContainer = Container(
                        child: Text(
                          "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                          maxLines: 5,
                          style: TextStyle(
                            overflow: TextOverflow.ellipsis,
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      );
                    }

                    return Container(
                      decoration: BoxDecoration(
                        border: Border(
                          bottom: index != 1
                              ? BorderSide(
                                  color: Color(0xfffF3F6F6), width: 10.w)
                              : BorderSide(width: 0, color: Colors.transparent),
                        ),
                      ),
                      child: Container(
                        padding: const EdgeInsets.all(15).w,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            if (index == 1)
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Wrap(
                                    crossAxisAlignment: WrapCrossAlignment.end,
                                    children: [
                                      Icon(
                                        Icons.upload_file,
                                        size: 15,
                                      ),
                                      SizedBox(
                                        width: 5.0.w,
                                      ),
                                      Text(
                                        "Charis",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                      SizedBox(
                                        width: 2.0.w,
                                      ),
                                      Text(
                                        "shared post",
                                        style: TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10.0.h,
                                  ),
                                  Container(
                                    height: 1.h,
                                    color: Colors.grey[200]!,
                                  ),
                                  SizedBox(
                                    height: 10.0.h,
                                  ),
                                ],
                              ),
                            Row(
                              children: [
                                CircleAvatar(
                                  backgroundColor: Colors.grey[300]!,
                                  radius: 28.0,
                                  child: Icon(
                                    Icons.person,
                                    color: Colors.grey[900]!,
                                    size: 28.0,
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Wrap(
                                      children: [
                                        Text(
                                          "Charis",
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xfff455154),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 1.0.w,
                                        ),
                                        Text(
                                          "is in",
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: Color(0xfff455154),
                                          ),
                                        ),
                                        SizedBox(
                                          width: 1.0.w,
                                        ),
                                        Icon(
                                          Icons.room,
                                          size: 20,
                                        ),
                                        Text(
                                          "Semarang, ID",
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xfff455154),
                                          ),
                                        ),
                                      ],
                                    ),
                                    SizedBox(
                                      height: 1.0.h,
                                    ),
                                    Text(
                                      "2 min ago",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                  ],
                                ),
                                const Spacer(),
                                Icon(
                                  Icons.more_horiz,
                                  color: Colors.grey[500]!,
                                  size: 20.0,
                                )
                              ],
                            ),
                            SizedBox(
                              height: 10.0.h,
                            ),
                            bodyContainer,
                            SizedBox(
                              height: 10.0.h,
                            ),
                            const Spacer(),
                            Container(
                              height: 1.h,
                              color: Colors.grey[200]!,
                            ),
                            SizedBox(
                              height: 10.0.h,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Icon(Icons.favorite),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Text(
                                  "999",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Icon(Icons.forum),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Text(
                                  "999",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Icon(Icons.share),
                                SizedBox(
                                  width: 10.0.w,
                                ),
                                Text(
                                  "999",
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Color(0xfff455154),
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<Variant6View> createState() => Variant6Controller();
}
