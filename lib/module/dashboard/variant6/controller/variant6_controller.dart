import 'package:flutter/material.dart';
import '../view/variant6_view.dart';

class Variant6Controller extends State<Variant6View> {
  static late Variant6Controller instance;
  late Variant6View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
