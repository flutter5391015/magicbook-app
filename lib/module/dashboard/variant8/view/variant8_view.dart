import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class Variant8View extends StatefulWidget {
  const Variant8View({Key? key}) : super(key: key);

  Widget build(context, Variant8Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: Container(
          padding: const EdgeInsets.only(left: 16, right: 16, top: 5).w,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Image.network(
                    "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                    width: 32.w,
                    height: 32.h,
                    fit: BoxFit.contain,
                  ),
                  SizedBox(
                    width: 10.0.w,
                  ),
                  Text(
                    "Company Name",
                    style: TextStyle(
                      fontSize: 20.0.sp,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  const Spacer(),
                  CircleAvatar(
                    radius: 20.r,
                    backgroundColor: Colors.grey[200]!,
                    child: Icon(
                      Icons.person,
                      color: Colors.blueGrey[300]!,
                      size: 20.w,
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: 5.0.h,
              ),
              TabBar(
                controller: controller.tabController,
                indicatorColor: primaryColor,
                indicatorSize: TabBarIndicatorSize.tab,
                indicatorWeight: 2,
                labelStyle: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                ),
                labelColor:
                    primaryColor, // Ubah warna teks tab saat aktif menjadi hijau
                unselectedLabelColor: Color(0xfff455154),
                tabs: [
                  Tab(text: 'For You'),
                  Tab(text: 'Popular'),
                  Tab(text: 'Recent'),
                ],
              ),
              SizedBox(height: 10.h),
              Container(
                height: 1.sh -
                    kToolbarHeight -
                    MediaQuery.of(context).padding.top -
                    100.h,
                child: TabBarView(
                  controller: controller.tabController,
                  children: [
                    SingleChildScrollView(
                      child: Column(
                        children: [
                          GridView.builder(
                            padding: EdgeInsets.zero,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 1.0 / 1.55,
                              crossAxisCount: 2,
                              mainAxisSpacing: 6,
                              crossAxisSpacing: 6,
                            ),
                            itemCount: 2,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Stack(
                                      children: [
                                        Container(
                                          height: 160.0.h,
                                          decoration: const BoxDecoration(
                                            color: Color(0xfffCDCDCD),
                                            image: DecorationImage(
                                              image: NetworkImage(
                                                "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png",
                                              ),
                                              fit: BoxFit.contain,
                                            ),
                                            borderRadius: BorderRadius.all(
                                              Radius.circular(
                                                8.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Positioned(
                                          right: 10,
                                          top: 5,
                                          child: CircleAvatar(
                                            backgroundColor: Colors.white,
                                            radius: 20.0.r,
                                            child: Icon(
                                              Icons.bookmark,
                                              color: Colors.grey[900]!,
                                              size: 20.0.r,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                    SizedBox(
                                      height: 14.0.h,
                                    ),
                                    Text(
                                      "14 Juli 2020",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0.h,
                                    ),
                                    Expanded(
                                      child: Text(
                                        "If Michael Cohen Is Smart, There's More Than One Tape",
                                        maxLines: 4,
                                        style: TextStyle(
                                          overflow: TextOverflow.ellipsis,
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0.h,
                                    ),
                                    Text(
                                      "By Charis",
                                      style: TextStyle(
                                        fontSize: 12.0,
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                          SizedBox(
                            height: 16.0.h,
                          ),
                          GridView.builder(
                            padding: EdgeInsets.zero,
                            gridDelegate:
                                SliverGridDelegateWithFixedCrossAxisCount(
                              childAspectRatio: 2.5 / 1.0,
                              crossAxisCount: 1,
                              crossAxisSpacing: 1,
                              mainAxisSpacing: 2,
                            ),
                            itemCount: 10,
                            shrinkWrap: true,
                            physics: ScrollPhysics(),
                            itemBuilder: (BuildContext context, int index) {
                              return Container(
                                decoration: BoxDecoration(
                                  border: Border(
                                    top: index == 0
                                        ? BorderSide(
                                            width: 1,
                                            color: Color(0xfffE6E9EB),
                                          )
                                        : BorderSide(color: Colors.transparent),
                                    bottom: index != 9
                                        ? BorderSide(
                                            width: 1,
                                            color: Color(0xfffE6E9EB),
                                          )
                                        : BorderSide(color: Colors.transparent),
                                  ),
                                ),
                                padding: EdgeInsets.symmetric(vertical: 15.h),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Container(
                                      width: 100.w,
                                      decoration: BoxDecoration(
                                        color: Colors.grey[100]!,
                                        image: DecorationImage(
                                          image: NetworkImage(
                                            "https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/Image_not_available.png/640px-Image_not_available.png",
                                          ),
                                          fit: BoxFit.contain,
                                        ),
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(
                                            8.0,
                                          ),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width: 15.0.w,
                                    ),
                                    Container(
                                      width: Get.width,
                                      height: Get.height,
                                      constraints: BoxConstraints(
                                        maxWidth:
                                            MediaQuery.of(context).size.width -
                                                150.w,
                                      ),
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            top: 0,
                                            bottom: 0,
                                            left: 0,
                                            right: 0,
                                            child: Container(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    "14 Juli 2020",
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 7.0.h,
                                                  ),
                                                  Text(
                                                    "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                                                    maxLines: 2,
                                                    style: TextStyle(
                                                      overflow:
                                                          TextOverflow.ellipsis,
                                                      fontSize: 16.0,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                  SizedBox(
                                                    height: 5.0.h,
                                                  ),
                                                  Text(
                                                    "By Charis",
                                                    style: TextStyle(
                                                      fontSize: 12.0,
                                                      fontWeight:
                                                          FontWeight.w400,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                          Positioned(
                                            top: 0,
                                            right: 0,
                                            child: CircleAvatar(
                                              backgroundColor:
                                                  Color(0xfffCDCDCD),
                                              radius: 16.0,
                                              child: Icon(
                                                Icons.bookmark,
                                                color: Colors.grey[900]!,
                                                size: 16.0,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              );
                            },
                          ),
                        ],
                      ),
                    ),
                    // Konten untuk Tab 'Popular'
                    Center(
                      child: Text('Popular Content'),
                    ),
                    // Konten untuk Tab 'Recent'
                    Center(
                      child: Text('Recent Content'),
                    ),
                  ],
                ),
              ), // Ber
            ],
          ),
        ),
      ),
    );
  }

  @override
  State<Variant8View> createState() => Variant8Controller();
}
