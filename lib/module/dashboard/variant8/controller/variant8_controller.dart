import 'package:flutter/material.dart';
import '../view/variant8_view.dart';

class Variant8Controller extends State<Variant8View>
    with SingleTickerProviderStateMixin {
  static late Variant8Controller instance;
  late Variant8View view;
  late TabController _tabController;

  @override
  void initState() {
    instance = this;

    super.initState();
    _tabController = TabController(length: 3, initialIndex: 0, vsync: this);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) => widget.build(context, this);

  TabController get tabController => _tabController; //
}
