import 'package:flutter/material.dart';
import '../view/variant10_view.dart';

class Variant10Controller extends State<Variant10View> {
  static late Variant10Controller instance;
  late Variant10View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
