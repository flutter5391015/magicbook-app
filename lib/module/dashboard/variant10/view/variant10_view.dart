import 'package:flutter/material.dart';
import 'package:magicbook_app/core.dart';
import '../controller/variant10_controller.dart';

class Variant10View extends StatefulWidget {
  const Variant10View({Key? key}) : super(key: key);

  Widget build(context, Variant10Controller controller) {
    controller.view = this;

    return Scaffold(
      appBar: AppBar(
        title: const Text("Variant10"),
        actions: const [],
      ),
      body: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: const [],
          ),
        ),
      ),
    );
  }

  @override
  State<Variant10View> createState() => Variant10Controller();
}
