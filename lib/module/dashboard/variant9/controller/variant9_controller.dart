import 'package:flutter/material.dart';
import '../view/variant9_view.dart';

class Variant9Controller extends State<Variant9View> {
  static late Variant9Controller instance;
  late Variant9View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
