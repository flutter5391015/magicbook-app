import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class Variant2View extends StatefulWidget {
  const Variant2View({Key? key}) : super(key: key);

  Widget build(context, Variant2Controller controller) {
    controller.view = this;

    return Scaffold(
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            child: Column(
              children: [
                Padding(
                  padding:
                      EdgeInsets.symmetric(vertical: 5.0.h, horizontal: 16.0.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Image.network(
                        "https://cdn-icons-png.flaticon.com/128/3585/3585972.png",
                        width: 32.w,
                        height: 32.h,
                        fit: BoxFit.contain,
                      ),
                      SizedBox(
                        width: 10.0.w,
                      ),
                      Text(
                        "Company Name",
                        style: TextStyle(
                          fontSize: 20.0.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      const Spacer(),
                      CircleAvatar(
                        radius: 20.r,
                        backgroundColor: Colors.grey[200]!,
                        child: Icon(
                          Icons.person,
                          color: Colors.blueGrey[300]!,
                          size: 20.w,
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 16.0.h,
                ),
                Builder(builder: (context) {
                  return CarouselSlider(
                    options: CarouselOptions(
                      height: 160.0,
                      autoPlay: true,
                      enlargeCenterPage: false,
                    ),
                    items: controller.images.map((imageUrl) {
                      return Builder(
                        builder: (BuildContext context) {
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            margin: EdgeInsets.symmetric(horizontal: 5.0).w,
                            decoration: BoxDecoration(
                              color: Colors.transparent,
                              border: Border.all(
                                color: Colors.grey[400]!,
                              ),
                              borderRadius: BorderRadius.all(
                                Radius.circular(6.0).r,
                              ),
                              image: DecorationImage(
                                image: NetworkImage(
                                  imageUrl,
                                ),
                                fit: BoxFit.cover,
                              ),
                            ),
                          );
                        },
                      );
                    }).toList(),
                  );
                }),
                SizedBox(
                  height: 16.0.h,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 16.0.w),
                  margin: EdgeInsets.only(bottom: 40.h),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      H3(title: 'Title', subtitle: 'See All'),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      GridView.builder(
                        padding: EdgeInsets.zero,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 4.0 / 1.0,
                          crossAxisCount: 2,
                          mainAxisSpacing: 10,
                          crossAxisSpacing: 6,
                        ),
                        itemCount: 4,
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            padding: EdgeInsets.symmetric(horizontal: 10).w,
                            decoration: BoxDecoration(
                              color: Colors.grey[300],
                              borderRadius: BorderRadius.circular(8).r,
                            ),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Expanded(
                                  child: Text(
                                    "item ${index + 1}",
                                    style: TextStyle(
                                      fontSize: 14.0,
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () {},
                                  child: Icon(
                                    Icons.chevron_right,
                                  ),
                                )
                              ],
                            ),
                          );
                        },
                      ),
                      SizedBox(
                        height: 16.0.h,
                      ),
                      H3(title: 'Title', subtitle: 'See All'),
                      SizedBox(
                        height: 10.0.h,
                      ),
                      GridView.builder(
                        padding: EdgeInsets.zero,
                        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          childAspectRatio: 1.0 / 1.4,
                          crossAxisCount: 2,
                          mainAxisSpacing: 24,
                          crossAxisSpacing: 6,
                        ),
                        itemCount: 4,
                        shrinkWrap: true,
                        physics: ScrollPhysics(),
                        itemBuilder: (BuildContext context, int index) {
                          return Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Expanded(
                                  child: Container(
                                    width: Get.width,
                                    decoration: BoxDecoration(
                                      color: Colors.grey[300]!,
                                      image: DecorationImage(
                                        image: NetworkImage(
                                          "https://static.thenounproject.com/png/1723279-200.png",
                                        ),
                                        fit: BoxFit.cover,
                                      ),
                                      borderRadius: BorderRadius.all(
                                        Radius.circular(
                                          8.0,
                                        ),
                                      ),
                                    ),
                                    child: Stack(
                                      children: [
                                        Positioned(
                                          right: 10,
                                          top: 10,
                                          child: CircleAvatar(
                                            radius: 20.r,
                                            backgroundColor: Colors.white,
                                            child: Icon(
                                              color: Colors.grey[500]!,
                                              Icons.more_horiz,
                                            ),
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  height: 10.0.h,
                                ),
                                Text(
                                  "Title",
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff455154),
                                  ),
                                ),
                                SizedBox(
                                  height: 3.0.h,
                                ),
                                Text(
                                  "Subtitle",
                                  style: TextStyle(
                                    fontSize: 12.0,
                                    color: Color(0xff969FA2),
                                  ),
                                ),
                                SizedBox(
                                  height: 3.0.h,
                                ),
                                Text(
                                  "Rp 1000",
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    fontWeight: FontWeight.bold,
                                    color: Color(0xff455154),
                                  ),
                                ),
                              ],
                            ),
                          );
                        },
                      ),
                    ],
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<Variant2View> createState() => Variant2Controller();
}
