import 'package:flutter/material.dart';
import '../view/variant4_view.dart';

class Variant4Controller extends State<Variant4View> {
  static late Variant4Controller instance;
  late Variant4View view;

  @override
  void initState() {
    instance = this;
    super.initState();
  }

  @override
  void dispose() => super.dispose();

  @override
  Widget build(BuildContext context) => widget.build(context, this);
}
