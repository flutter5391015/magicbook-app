import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:magicbook_app/core.dart';

class Variant4View extends StatefulWidget {
  const Variant4View({Key? key}) : super(key: key);

  Widget build(context, Variant4Controller controller) {
    controller.view = this;

    return Scaffold(
      backgroundColor: Colors.white,
      body: SafeArea(
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  padding:
                      EdgeInsets.symmetric(horizontal: 16.w, vertical: 5.h),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        "Feed",
                        style: TextStyle(
                          fontSize: 30.0.sp,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(
                        height: 16.0.h,
                      ),
                      Container(
                        height: 44.h,
                        padding: EdgeInsets.symmetric(
                          horizontal: 1.0.w,
                        ),
                        decoration: BoxDecoration(
                          color: Colors.grey[300],
                          borderRadius: BorderRadius.all(
                            Radius.circular(
                              6.0.r,
                            ),
                          ),
                        ),
                        child: Center(
                          child: TextField(
                            style: TextStyle(
                              backgroundColor: Colors.transparent,
                              color: Colors.grey[800],
                            ),
                            decoration: InputDecoration(
                              filled: false,
                              enabledBorder: InputBorder.none,
                              hintText: "Search",
                              prefixIcon: Icon(
                                Icons.search,
                                color: Colors.grey[800],
                              ),
                              suffixIcon: Icon(
                                Icons.mic,
                                color: Colors.grey[800],
                              ),
                              border: InputBorder.none,
                              hintStyle: TextStyle(
                                color: Colors.grey[800],
                              ),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 10.0.h,
                      ),
                    ],
                  ),
                ),
                Container(
                  child: SingleChildScrollView(
                    scrollDirection: Axis.horizontal,
                    controller: ScrollController(),
                    child: Row(
                      children: List.generate(
                        10,
                        (index) {
                          return Row(
                            children: [
                              if (index == 0) ...[
                                Container(
                                  height: 150.h,
                                  width: 90.w,
                                  padding: EdgeInsets.symmetric(
                                    vertical: 10.0.h,
                                  ),
                                  margin: EdgeInsets.only(
                                      left: (index == 0 ? 16 : 0), right: 10),
                                  decoration: BoxDecoration(
                                    color: Colors.grey[400]!,
                                    borderRadius: const BorderRadius.all(
                                      Radius.circular(16.0),
                                    ),
                                  ),
                                  child: Column(
                                    children: [
                                      Spacer(),
                                      CircleAvatar(
                                        backgroundColor: primaryColor,
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                        ),
                                      ),
                                      SizedBox(
                                        height: 5.0.h,
                                      ),
                                      Text(
                                        "Add Story",
                                        style: const TextStyle(
                                          color: Colors.black,
                                          fontSize: 11.0,
                                        ),
                                      ),
                                    ],
                                  ),
                                )
                              ],
                              Container(
                                height: 150.h,
                                width: 90.w,
                                padding: EdgeInsets.symmetric(
                                  vertical: 10.0.h,
                                ),
                                margin: EdgeInsets.only(
                                    right: (index + 1 == 10 ? 16 : 10)),
                                decoration: BoxDecoration(
                                  color: Colors.grey[400]!,
                                  borderRadius: const BorderRadius.all(
                                    Radius.circular(16.0),
                                  ),
                                ),
                                child: Column(
                                  children: [
                                    Spacer(),
                                    CircleAvatar(
                                      backgroundColor: Colors.white,
                                      child: Icon(
                                        Icons.person_2,
                                        color: Color(0xfff455154),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 5.0.h,
                                    ),
                                    Text(
                                      "Story ${index + 1}",
                                      style: const TextStyle(
                                        color: Colors.black,
                                        fontSize: 11.0,
                                      ),
                                    ),
                                  ],
                                ),
                              )
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 40.0.h,
                ),
                Container(
                  child: GridView.builder(
                    padding: EdgeInsets.zero,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      childAspectRatio: 1.8 / 1.0,
                      crossAxisCount: 1,
                    ),
                    itemCount: 4,
                    shrinkWrap: true,
                    physics: ScrollPhysics(),
                    itemBuilder: (BuildContext context, int index) {
                      BorderSide border = BorderSide(
                        color: index % 2 == 0
                            ? Color(0xfffE6E9EB)
                            : Colors.transparent,
                        width: 2,
                      );
                      BorderSide borderBottom = BorderSide(
                        color: index == 3
                            ? Color(0xfffE6E9EB)
                            : Colors.transparent,
                        width: 2,
                      );

                      return Container(
                        decoration: BoxDecoration(
                          border: Border(
                            top: border,
                            bottom: index == 3 ? borderBottom : border,
                          ),
                        ),
                        child: Container(
                          padding: EdgeInsets.all(15).w,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Row(
                                children: [
                                  CircleAvatar(
                                    backgroundColor: Color(0xfffCDCDCD),
                                    radius: 16.0.r,
                                    child: Icon(
                                      Icons.person,
                                      color: Color(0xfff455154),
                                      size: 20.0,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0.w,
                                  ),
                                  Expanded(
                                    child: Text(
                                      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        fontSize: 14.0.sp,
                                        fontWeight: FontWeight.w700,
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 3.0.w,
                                  ),
                                  Icon(
                                    Icons.check_circle,
                                    size: 15,
                                  ),
                                  const Spacer(),
                                  Text(
                                    "2 min ago",
                                    style: TextStyle(
                                      fontSize: 12.0,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0.w,
                                  ),
                                  Icon(Icons.more_horiz),
                                ],
                              ),
                              SizedBox(
                                height: 16.0.h,
                              ),
                              Text(
                                "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
                                maxLines: 3,
                                overflow: TextOverflow.ellipsis,
                                style: TextStyle(
                                  fontSize: 24.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              SizedBox(
                                height: 16.0.h,
                              ),
                              Row(
                                children: [
                                  Text(
                                    "89.4K likes",
                                    style: TextStyle(
                                      fontSize: 14.0.sp,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.0.w,
                                  ),
                                  Text(
                                    "93 comments",
                                    style: TextStyle(
                                      fontSize: 14.0.sp,
                                    ),
                                  ),
                                  const Spacer(),
                                  CircleAvatar(
                                    backgroundColor: Color(0xffF3F6F6),
                                    radius: 16.0.r,
                                    child: Icon(
                                      Icons.favorite,
                                      color: Color(0xfff455154),
                                      size: 20.0,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0.w,
                                  ),
                                  CircleAvatar(
                                    backgroundColor: Color(0xffF3F6F6),
                                    radius: 16.0.r,
                                    child: Icon(
                                      Icons.message,
                                      color: Color(0xfff455154),
                                      size: 20.0,
                                    ),
                                  ),
                                  SizedBox(
                                    width: 10.0.w,
                                  ),
                                  CircleAvatar(
                                    backgroundColor: Color(0xffF3F6F6),
                                    radius: 16.0.r,
                                    child: Icon(
                                      Icons.share,
                                      color: Color(0xfff455154),
                                      size: 20.0,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  @override
  State<Variant4View> createState() => Variant4Controller();
}
